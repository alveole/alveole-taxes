# alveole-taxes
Use this module to generate various tax objects and find tax information for each of the provinces in Canada.

This module uses a fluent API, calls that alter the object's state are chain-able:
    
    var taxes = require('./taxes');
    
    var tax = taxes()
        .country('can')
        .region('on');
        
    var quickBooksData = tax.quickBooksObject();

    //
    taxes().country(<country code>)
        // <country code> should be an ISO ALPHA-3 country code.
    
    taxes().region(<region code>)
        // <region code> is the region code as defined in taxData.
        
    taxes().quickBooksObject()
        // returns the tax data object required to sync to quickbooks.
        // requires that the country is set.
