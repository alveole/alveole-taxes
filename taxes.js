(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
    typeof define === 'function' && define.amd ? define(factory) :
    global._taxes = factory();
}(this, (function () { 'use strict';
    var proto = {
        isArray: function(input) {
            return input instanceof Array || Object.prototype.toString.call(input) === '[object Array]';
        },

        // Use ISO ALPHA-3 country codes.
        // Find tax codes here: 
        // https://developer.intuit.com/v2/apiexplorer?apiname=V3QBO#?id=TaxCode
        // using queries like `select * from taxcode where name like '%C.-B.%'` -- for BC for example.
        // The id of the TaxCode Object is the ref and the value in TaxRateRef is the rate.
        // eg:
        // Each use-able sub tax has it's own TaxCode object to get the Id from for the ref.
        // codes: {
        //  rate: TaxCode.TaxRateDetails.TaxRateRef.value
        //  ref: TaxCode.Id
        // }
        taxData: {
            CAN: {
                types: ['HST', 'GST', 'PST', 'QST'],
                exempt: { type: 'Tax-exempt', rate: 0, codes: { rate: '2', ref: '2', level: 'fed' } },
                gst: { type: 'GST', rate: 5, codes: { rate: '10', ref: '5', level: 'fed', invoice: '818903775' } },
                typeOverrides: {
                    GST: function(types) {
                        if (types.indexOf('HST') !== -1)
                            return 'HST';
                        return 'GST';
                    },
                    'Tax-exempt': function(types) {
                        return '';
                    },
                },
                regions: ['AB', 'BC', 'MB', 'NB', 'NL', 'NS', 'NT', 'NU', 'ON', 'PE', 'QC', 'SK', 'YT'],
                regionalTaxes: {
                    AB: { type: 'Tax-exempt', rate: 0, codes: { rate: '2', ref: '2', level: 'fed' } },
                    BC: { type: 'PST', rate: 7, codes: { rate: '31', ref: '15', level: 'prov' } }, // ref comes from combined codes
                    MB: { type: 'PST', rate: 8, codes: { rate: '41', ref: '?', level: 'prov' } }, // ref comes from combined codes
                    NB: { type: 'HST', rate: 10, codes: { rate: '?', ref: '?', level: 'prov' } }, // ref and rate come from combined codes
                    NL: { type: 'HST', rate: 10, codes: { rate: '?', ref: '?', level: 'prov' } }, // ref and rate come from combined codes
                    NS: { type: 'HST', rate: 10, codes: { rate: '?', ref: '?', level: 'prov' } }, // ref and rate come from combined codes
                    NT: { type: 'Tax-exempt', rate: 0, codes: { rate: '2', ref: '2', level: 'fed' } },
                    NU: { type: 'Tax-exempt', rate: 0, codes: { rate: '2', ref: '2', level: 'fed' } },
                    ON: { type: 'HST', rate: 8, codes: { rate: '?', ref: '?', level: 'prov', invoice: '818903775' } }, // ref and rate come from combined codes
                    PE: { type: 'HST', rate: 10, codes: { rate: '?', ref: '?', level: 'prov' } }, // ref and rate come from combined codes
                    QC: { type: 'QST', rate: 9.975, codes: { rate: '25', ref: '13', level: 'prov', invoice: '1221281159' } },
                    SK: { type: 'PST', rate: 6, codes: { rate: '60', ref: '?', level: 'prov' } }, // ref comes from combined codes
                    YT: { type: 'Tax-exempt', rate: 0, codes: { rate: '2', ref: '2', level: 'fed' } },
                },
                combinedCodes: {
                    AB: { rate: '10', ref: '5', level: 'fed' },
                    BC: { rate: '??', ref: '17', level: 'fed' }, // rate codes come from regional-taxes
                    MB: { rate: '??', ref: '20', level: 'fed' }, // rate codes come from regional-taxes
                    NB: { rate: '46', ref: '24', level: 'fed' },
                    NL: { rate: '52', ref: '27', level: 'fed' },
                    NS: { rate: '54', ref: '28', level: 'fed' },
                    NT: { rate: '10', ref: '5', level: 'fed' },
                    NU: { rate: '10', ref: '5', level: 'fed' },
                    ON: { rate: '29', ref: '14', level: 'fed', invoice: '818903775' },
                    PE: { rate: '58', ref: '30', level: 'fed' },
                    QC: { rate: '??', ref: '10', level: 'fed' }, // rate codes come from regional-taxes
                    SK: { rate: '??', ref: '31', level: 'fed' }, // rate codes come from regional-taxes
                    YT: { rate: '10', ref: '5', level: 'fed' },
                },
            },
        },

        _country: null,        
        _region: null,
        _taxes: null,
        _gst: null,
        _regionalTax: null,
        _combinedTax: null,

        // check that all of the data is well formatted, adding this here
        // so that when we add other countries and more tax information
        // we will get very obvious errors if something is wrong.
        checkTax: function(tax) {
            if (!this._country) throw new Error('Country must be set. Please call .country(<country>) first.');

            var rate = tax.rate,
                type = tax.type,
                codes = tax.codes;

            if (!type) throw new Error('type is required for a tax');
            if (!(this._taxes && this._taxes.types)) throw new Error('taxes and tax types must be set.');
            var types = this._taxes.types;
            if (!types.indexOf(type) === -1) throw new Error('type must be one of ' + types.join(', '));
            if (isNaN(rate)) throw new Error('rate is required for a tax');
            if (!codes) throw new Error('codes is required for a tax');

            return true;
        },

        country: function(countryIn) {
            var t = this;

            if (typeof countryIn !== 'string') throw new Error('countryIn must be a string.');
            if (countryIn.length !== 3) throw new Error('countryIn must be a 3 character ISO Alpha-3 country code.')
            countryIn = countryIn.toUpperCase();
            if (!this.taxData.hasOwnProperty(countryIn)) throw new Error('We don\'t have taxes for: ' + countryIn);

            this._country = countryIn;
            this._taxes = this.taxData[countryIn];
            this._gst = this._taxes.gst;
            this.checkTax(this._taxes.exempt);
            this.checkTax(this._taxes.gst);
            this._taxes.regions.forEach(function(region) {
                var tax = t._taxes.regionalTaxes[region];
                t.checkTax(tax);
            });

            this._region = null;
            return this;
        },

        region: function(regionIn) {
            var t = this;
            function setRegion(input) {
                if (typeof input !== 'string') throw new Error('regionIn must be a string');
                if (typeof t._country !== 'string') throw new Error('country must be set to set the region. Please call .country(<countryCode>) first.');
                input = input.toUpperCase();
                if(!t._taxes.regionalTaxes.hasOwnProperty(input)) throw new Error('There is no tax information for: '+ input);
        
                t._region = input;
                t._regionalTax = t._taxes.regionalTaxes[input];
            }

            function setCombinedTaxes() {
                if (!t._region) throw new Error('Region must be set. Please call .region(<region>) first.');

                function combineTaxTypesStrings(types) {
                    return types.map(function(type) {
                        if (t._taxes.typeOverrides.hasOwnProperty(type)) {
                            return t._taxes.typeOverrides[type](types);
                        }
                        return type;
                    }).filter(function(type, index, arr) {
                        // unique
                        return arr.indexOf(type) === index && type !== '';
                    }).join('/');
                }

                // TODO this only works for a gst + regional tax system.
                var taxes = [t._regionalTax, t._gst];
                t._combinedTax = taxes.reduce(function(combined, tax) {
                    var rate = tax.rate || 0,
                        type = tax.type || '';

                    var type = combineTaxTypesStrings([combined.type, type]);
                    // this code is broken for more than two taxes
                    // and for combinations that aren't based on HST
                    var combinedTax = type === 'HST' || type === 'GST'
                        ? [{
                            rate: combined.rate + rate,
                            type: type,
                            codes: t._taxes.combinedCodes[t._region],
                        }]
                        : (combined.taxes || [combined]).concat(tax)

                    return {
                        rate: combined.rate + rate,
                        type: type,
                        codes: t._taxes.combinedCodes[t._region],
                        taxes: combinedTax,
                    };
                });
            }

            setRegion(regionIn);
            setCombinedTaxes(regionIn);
            return this;
        },

        unsetRegion: function() {
            this._region = null;
            this._combinedTax = null;
            this._regionalTax = null;
        },

        quickBooksObject: function() {
            var t = this;
            if (!this._country) throw new Error('Country must be set. Please call .country(<country>) first.');

            function generateSubObject(tax) {
                function generateTaxObject(taxInner) {
                    return {
                        taxRateRef: taxInner.codes.rate,
                        taxPercent: taxInner.rate,
                        taxType: taxInner.codes.level,
                    };
                }

                return {
                    taxCodeRef: tax.codes.ref,
                    percentTotal: tax.rate,
                    taxes: t.isArray(tax.taxes)
                        ? tax.taxes.map(generateTaxObject)
                        : [generateTaxObject(tax)],
                };
            }

            var taxObject = {};
            var gstSubObject = generateSubObject(this._taxes.gst);
            var exempt = generateSubObject(this._taxes.exempt);

            var region = this._region;
            this._taxes.regions.forEach(function(region) {
                t.region(region);
                taxObject[region] = {
                    // this is kinda stupid but its what the quickbooks sync wants
                    provOnly: generateSubObject(t._combinedTax),
                    fedOnly: gstSubObject,
                    both: generateSubObject(t._combinedTax),
                    none: exempt,
                };
            });
            if (region) this.region(region);
            else this.unsetRegion();
            return taxObject;
        },

        invoiceObjects: function(english) {
            if (!this._region) throw new Error('Region must be set before querying an invoice object. Please call .region(<region>) first.');
            // default to english
            english = typeof english === 'undefined' ? true : english;
            var codeToFrench = {
                GST: 'TPS',
                QST: 'TVQ',
            };
            return this._combinedTax.taxes.filter(function(tax) {
                return tax.type !== 'Tax-exempt';
            }).map(function(tax) {
                var type = !english && codeToFrench.hasOwnProperty(tax.type)
                    ? codeToFrench[tax.type]
                    : tax.type;
                var invoiceCode = tax.codes.invoice ? ('(#'+tax.codes.invoice+')') : '';
                var typeString = 'tax' + tax.codes.level.charAt(0).toUpperCase() + tax.codes.level.substr(1);

                return {
                    title: [type, invoiceCode].join(' ').trim(),
                    rate: parseFloat((tax.rate / 100).toFixed(5)),
                    type: typeString,
                };
            });
        },

        combined: function() {
            if (!this._region) throw new Error('Region must be set before querying an invoice object. Please call .region(<region>) first.');
            return this._combinedTax;
        },

        calculate: function(taxableAmounts) {
            if (!taxableAmounts.hasOwnProperty('regional')) throw new Error('taxableAmounts must have a regional property.');
            if (!taxableAmounts.hasOwnProperty('federal')) throw new Error('taxableAmounts must have a federal property.');
            var regionalAmount = taxableAmounts.regional,
                federalAmount = taxableAmounts.federal;

            if (isNaN(regionalAmount) || regionalAmount < 0) throw new Error('regional must be a non-negative number.');
            if (isNaN(federalAmount) || federalAmount < 0) throw new Error('federal must be a non-negative number.');

            var regionalRate = this._regionalTax.rate;
            var federalRate = this._gst.rate;

            var regionalTax = parseFloat((regionalRate / 100)) * regionalAmount;
            var federalTax = parseFloat((federalRate / 100)) * federalAmount;

            return {
                regional: regionalTax,
                federal: federalTax,
                total: regionalTax + federalTax,
            };
        },

        rate: function() {
            return this.combined().rate;
        }
    };

    return function() {
        return Object.create(proto);
    };

})));