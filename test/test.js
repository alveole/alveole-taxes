const expect = require('chai').expect;
const taxes = require('../taxes');

describe('require(\'./taxes\')', function() {
    it('should be a factory function', function() {
        expect(taxes).to.be.a('function');
    });

    it('should return an object', function() {
        expect(taxes()).to.be.an('object');
    });
});

describe('taxes.country', function() {
    it('should be a function', function() {
        expect(taxes().country).to.be.a('function');
    });

    it('should set the country', function() {
        const countrySet = taxes().country('can');
        expect(countrySet).to.have.a.property('_country');
        expect(countrySet._country).to.be.a('string');
    });
});

describe('taxes.region', function() {
    it('should require the country to be set', function() {
        const taxesObject = taxes();
        expect(taxesObject.region.bind(taxesObject, '109238')).to.throw('country must be set to set the region. Please call .country(<countryCode>) first.');
    });
    it('should accept only strings', function() {
        const taxesObject = taxes();
        expect(taxesObject.country('can').region.bind(taxesObject, 109238)).to.throw('regionIn must be a string');
    });
    it('should only accept provinces in the region', function() {
        const taxesObject = taxes();
        expect(taxesObject.country('can').region.bind(taxesObject, 'helloworld')).to.throw('There is no tax information for: HELLOWORLD')
    });
    it('should accept upper and lowercase arguments', function() {
        const taxesObject = taxes();
        const lower = taxesObject.country('can').region('on');
        const upper = taxesObject.country('CAN').region('ON');
        expect(lower).to.deep.equal(upper);
    });
    it('should set combinedTax', function() {
        const onTaxes = taxes().country('can').region('ON');
        expect(onTaxes).to.have.a.property('_combinedTax');
        expect(onTaxes._combinedTax).to.be.an('object');
    });
});

describe('taxes.combined', function() {
    it('should return an object', function() {
        const combined = taxes().country('can').region('ON').combined();
        expect(combined).to.be.an('object');
    });
});

describe('taxes.invoiceObject', function() {
    it('should return multiple taxes for states with multiple taxes', function() {
        const regionsWithTwoTaxes = ['sk', 'qc', 'mb', 'bc'];
        regionsWithTwoTaxes.forEach(function(region) {
            expect(taxes().country('can').region(region).invoiceObjects()).to.have.lengthOf(2);
        });
    });

    it('should combine taxes for states with HST', function() {
        const regionsWithHST = ['on', 'nb', 'ns', 'pe', 'nl'];
        regionsWithHST.forEach(function(region) {
            expect(taxes().country('can').region(region).invoiceObjects()).to.have.lengthOf(1);
        });
    });

    it('should return an array like this for ON', function() {
        const invoiceObjectON = [{
            title: 'HST (#818903775)',
            rate: 0.13,
            type: 'taxFed'
        }];
        expect(taxes().country('can').region('on').invoiceObjects()).to.deep.equal(invoiceObjectON);
    });

    it('should return an array like this for QC english and it should default to english', function() {
        const invoiceObjectQCEn = [
            {
                title: 'QST (#1221281159)',
                rate: 0.09975,
                type: 'taxProv'
            },
            {
                title: 'GST (#818903775)',
                rate: 0.05,
                type: 'taxFed'
            }
        ];
        expect(taxes().country('can').region('qc').invoiceObjects()).to.deep.equal(invoiceObjectQCEn);
        expect(taxes().country('can').region('qc').invoiceObjects(true)).to.deep.equal(invoiceObjectQCEn);
    });

    it('should return an array like this for QC french', function() {
        const invoiceObjectQCFr = [
            {
                title: 'TVQ (#1221281159)',
                rate: 0.09975,
                type: 'taxProv'
            },
            {
                title: 'TPS (#818903775)',
                rate: 0.05,
                type: 'taxFed'
            }
        ];
        expect(taxes().country('can').region('qc').invoiceObjects(false)).to.deep.equal(invoiceObjectQCFr);
    });

    it('should return an array like this for NT', function() {
        const invoiceObjectNT = [{
            title: 'GST',
            rate: 0.05,
            type: 'taxFed'
        }];
        expect(taxes().country('can').region('nt').invoiceObjects()).to.deep.equal(invoiceObjectNT);
    });
});

describe('taxes.calculate', function() {
    it('should require an object argument that has regional and federal properties', function() {
        const goodArgs = { federal: 0, regional: 0 };
        const missingFederalArgs = { regional: 0 };
        const missingRegionalArgs = { federal: 0 };
        const onTaxes = taxes().country('can').region('ON');

        expect(onTaxes.calculate(goodArgs)).to.deep.equal({ federal: 0, regional: 0, total: 0 });
        expect(onTaxes.calculate.bind(onTaxes, missingFederalArgs)).to.throw('taxableAmounts must have a federal property.');
        expect(onTaxes.calculate.bind(onTaxes, missingRegionalArgs)).to.throw('taxableAmounts must have a regional property.');
    });

    it('should not accept negative taxable amounts', function() {
        const negativeRegional = { federal: 0, regional: -1 };
        const negativeFederal = { federal: -1, regional: 0 };
        const onTaxes = taxes().country('can').region('ON');
        expect(onTaxes.calculate.bind(onTaxes, negativeRegional)).to.throw('regional must be a non-negative number.');
        expect(onTaxes.calculate.bind(onTaxes, negativeFederal)).to.throw('federal must be a non-negative number.');
    });

    it('should calculate taxes', function() {
        const testOne = { federal: 100, regional: 100 };
        const testTwo = { federal: 123.11, regional: 123.12 };
        const resultOne = taxes().country('can').region('on').calculate(testOne);
        const resultTwo = taxes().country('can').region('on').calculate(testTwo);

        expect(resultOne).to.deep.equal({ federal: 5, regional: 8, total: 13 });
        expect(resultTwo).to.deep.equal({ federal: 6.1555, regional: 9.8496, total: 16.0051 });
    });
});

describe('taxes.quickbooksObject', function() {
    it('should return an object like this for QC', function() {
        const quickbooksObjectQC = {
            provOnly: {
                taxCodeRef: '10', // QST only
                percentTotal: 14.975,
                taxes: [
                    {
                        taxRateRef: '25',
                        taxPercent: 9.975,
                        taxType: 'prov'
                    },
                    {
                        taxPercent: 5,
                        taxRateRef: '10',
                        taxType: 'fed',
                    }
                ]
            },
            fedOnly: {
                taxCodeRef: '5',
                percentTotal: 5, // GST only
                taxes: [
                    {
                        taxRateRef: '10',
                        taxPercent: 5,
                        taxType: 'fed'
                    }
                ]
            },
            both: {
                taxCodeRef: '10', // GST/QST
                percentTotal: 14.975,
                taxes: [
                    {
                        taxRateRef: '25',
                        taxPercent: 9.975,
                        taxType: 'prov'
                    },
                    {
                        taxRateRef: '10',
                        taxPercent: 5,
                        taxType: 'fed'
                    }
                ]
            },
            none: {
                taxCodeRef: '2', // Tax-exempt
                percentTotal: 0,
                taxes: [
                    {
                        taxRateRef: '2',
                        taxPercent: 0,
                        taxType: 'fed'
                    }
                ]
            }
        };
        const resultObject = taxes().country('can').quickBooksObject().QC;
        expect(resultObject).to.deep.equal(quickbooksObjectQC);
    });

    it('should return an object like this for ON', function() {
        const quickbooksObjectON = {
            provOnly: {
                taxCodeRef: '14', // HST ON
                percentTotal: 13,
                taxes: [
                    {
                        taxRateRef: '29',
                        taxPercent: 13,
                        taxType: 'fed'
                    }
                ]
            },
            fedOnly: {
                taxCodeRef: '5',
                percentTotal: 5, // HST only
                taxes: [
                    {
                        taxRateRef: '10',
                        taxPercent: 5,
                        taxType: 'fed'
                    }
                ]
            },
            both: {
                taxCodeRef: '14', //HST ON
                percentTotal: 13,
                taxes: [
                    {
                        taxRateRef: '29',
                        taxPercent: 13,
                        taxType: 'fed'
                    }
                ]
            },
            none: {
                taxCodeRef: '2', // Tax-exempt
                percentTotal: 0,
                taxes: [
                    {
                        taxRateRef: '2',
                        taxPercent: 0,
                        taxType: 'fed'
                    }
                ]
            }
        };
        const resultObject = taxes().country('can').quickBooksObject().ON;
        expect(resultObject).to.deep.equal(quickbooksObjectON);
    });

    it('should return an object like this for NL', function() {
        const regions = ['NL'];
        const object = {
            provOnly: {
                taxCodeRef: '27', 
                percentTotal: 15,
                taxes: [
                    {
                        taxRateRef: '52',
                        taxPercent: 15,
                        taxType: 'fed'
                    }
                ]
            },
            fedOnly: {
                taxCodeRef: '5',
                percentTotal: 5, 
                taxes: [
                    {
                        taxRateRef: '10',
                        taxPercent: 5,
                        taxType: 'fed'
                    }
                ]
            },
            both: {
                taxCodeRef: '27', 
                percentTotal: 15,
                taxes: [
                    {
                        taxRateRef: '52',
                        taxPercent: 15,
                        taxType: 'fed'
                    }
                ]
            },
            none: {
                taxCodeRef: '2',
                percentTotal: 0,
                taxes: [
                    {
                        taxRateRef: '2',
                        taxPercent: 0,
                        taxType: 'fed'
                    }
                ]
            }
        };
        const outObject = taxes().country('can').quickBooksObject();
        regions.forEach(function(region) {
            expect(outObject[region]).to.deep.equal(object);
        });
    });

    it('should return an object like this for NS', function() {
        const regions = ['NS'];
        const object = {
            provOnly: {
                taxCodeRef: '28', 
                percentTotal: 15,
                taxes: [
                    {
                        taxRateRef: '54',
                        taxPercent: 15,
                        taxType: 'fed'
                    }
                ]
            },
            fedOnly: {
                taxCodeRef: '5',
                percentTotal: 5, 
                taxes: [
                    {
                        taxRateRef: '10',
                        taxPercent: 5,
                        taxType: 'fed'
                    }
                ]
            },
            both: {
                taxCodeRef: '28', 
                percentTotal: 15,
                taxes: [
                    {
                        taxRateRef: '54',
                        taxPercent: 15,
                        taxType: 'fed'
                    }
                ]
            },
            none: {
                taxCodeRef: '2',
                percentTotal: 0,
                taxes: [
                    {
                        taxRateRef: '2',
                        taxPercent: 0,
                        taxType: 'fed'
                    }
                ]
            }
        };
        const outObject = taxes().country('can').quickBooksObject();
        regions.forEach(function(region) {
            expect(outObject[region]).to.deep.equal(object);
        });
    });

    it('should return an object like this for NB', function() {
        const regions = ['NB'];
        const object = {
            provOnly: {
                taxCodeRef: '24', 
                percentTotal: 15,
                taxes: [
                    {
                        taxRateRef: '46',
                        taxPercent: 15,
                        taxType: 'fed'
                    }
                ]
            },
            fedOnly: {
                taxCodeRef: '5',
                percentTotal: 5, 
                taxes: [
                    {
                        taxRateRef: '10',
                        taxPercent: 5,
                        taxType: 'fed'
                    }
                ]
            },
            both: {
                taxCodeRef: '24', 
                percentTotal: 15,
                taxes: [
                    {
                        taxRateRef: '46',
                        taxPercent: 15,
                        taxType: 'fed'
                    }
                ]
            },
            none: {
                taxCodeRef: '2',
                percentTotal: 0,
                taxes: [
                    {
                        taxRateRef: '2',
                        taxPercent: 0,
                        taxType: 'fed'
                    }
                ]
            }
        };
        const outObject = taxes().country('can').quickBooksObject();
        regions.forEach(function(region) {
            expect(outObject[region]).to.deep.equal(object);
        });
    });

    it('should return an object like this for PE', function() {
        const regions = ['PE'];
        const object = {
            provOnly: {
                taxCodeRef: '30', 
                percentTotal: 15,
                taxes: [
                    {
                        taxRateRef: '58',
                        taxPercent: 15,
                        taxType: 'fed'
                    }
                ]
            },
            fedOnly: {
                taxCodeRef: '5',
                percentTotal: 5, 
                taxes: [
                    {
                        taxRateRef: '10',
                        taxPercent: 5,
                        taxType: 'fed'
                    }
                ]
            },
            both: {
                taxCodeRef: '30', 
                percentTotal: 15,
                taxes: [
                    {
                        taxRateRef: '58',
                        taxPercent: 15,
                        taxType: 'fed'
                    }
                ]
            },
            none: {
                taxCodeRef: '2',
                percentTotal: 0,
                taxes: [
                    {
                        taxRateRef: '2',
                        taxPercent: 0,
                        taxType: 'fed'
                    }
                ]
            }
        };
        const outObject = taxes().country('can').quickBooksObject();
        regions.forEach(function(region) {
            expect(outObject[region]).to.deep.equal(object);
        });
    });

    it('should return an object like this for BC', function() {
        const regions = ['BC'];
        const object = {
            provOnly: {
                taxCodeRef: '17', 
                percentTotal: 12,
                taxes: [
                    {
                        taxPercent: 7,
                        taxRateRef: '31',
                        taxType: 'prov',
                    },
                    {
                        taxRateRef: '10',
                        taxPercent: 5,
                        taxType: 'fed'
                    }
                ]
            },
            fedOnly: {
                taxCodeRef: '5',
                percentTotal: 5, 
                taxes: [
                    {
                        taxRateRef: '10',
                        taxPercent: 5,
                        taxType: 'fed'
                    }
                ]
            },
            both: {
                taxCodeRef: '17', 
                percentTotal: 12,
                taxes: [
                    {
                        taxPercent: 7,
                        taxRateRef: '31',
                        taxType: 'prov',
                    },
                    {
                        taxRateRef: '10',
                        taxPercent: 5,
                        taxType: 'fed'
                    }
                ]
            },
            none: {
                taxCodeRef: '2',
                percentTotal: 0,
                taxes: [
                    {
                        taxRateRef: '2',
                        taxPercent: 0,
                        taxType: 'fed'
                    }
                ]
            }
        };
        const outObject = taxes().country('can').quickBooksObject();
        regions.forEach(function(region) {
            expect(outObject[region]).to.deep.equal(object);
        });
    });

    it('should return an object like this for MB', function() {
        const regions = ['MB'];
        const object = {
            provOnly: {
                taxCodeRef: '20', 
                percentTotal: 13,
                taxes: [
                    {
                        taxPercent: 8,
                        taxRateRef: '41',
                        taxType: 'prov',
                    },
                    {
                        taxRateRef: '10',
                        taxPercent: 5,
                        taxType: 'fed'
                    }
                ]
            },
            fedOnly: {
                taxCodeRef: '5',
                percentTotal: 5, 
                taxes: [
                    {
                        taxRateRef: '10',
                        taxPercent: 5,
                        taxType: 'fed'
                    }
                ]
            },
            both: {
                taxCodeRef: '20', 
                percentTotal: 13,
                taxes: [
                    {
                        taxPercent: 8,
                        taxRateRef: '41',
                        taxType: 'prov',
                    },
                    {
                        taxRateRef: '10',
                        taxPercent: 5,
                        taxType: 'fed'
                    }
                ]
            },
            none: {
                taxCodeRef: '2',
                percentTotal: 0,
                taxes: [
                    {
                        taxRateRef: '2',
                        taxPercent: 0,
                        taxType: 'fed'
                    }
                ]
            }
        };
        const outObject = taxes().country('can').quickBooksObject();
        regions.forEach(function(region) {
            expect(outObject[region]).to.deep.equal(object);
        });
    });

    it('should return an object like this for SK', function() {
        const regions = ['SK'];
        const object = {
            provOnly: {
                taxCodeRef: '31', 
                percentTotal: 11,
                taxes: [
                    {
                        taxPercent: 6,
                        taxRateRef: '60',
                        taxType: 'prov',
                    },
                    {
                        taxRateRef: '10',
                        taxPercent: 5,
                        taxType: 'fed'
                    }
                ]
            },
            fedOnly: {
                taxCodeRef: '5',
                percentTotal: 5, 
                taxes: [
                    {
                        taxRateRef: '10',
                        taxPercent: 5,
                        taxType: 'fed'
                    }
                ]
            },
            both: {
                taxCodeRef: '31', 
                percentTotal: 11,
                taxes: [
                    {
                        taxPercent: 6,
                        taxRateRef: '60',
                        taxType: 'prov',
                    },
                    {
                        taxRateRef: '10',
                        taxPercent: 5,
                        taxType: 'fed'
                    }
                ]
            },
            none: {
                taxCodeRef: '2',
                percentTotal: 0,
                taxes: [
                    {
                        taxRateRef: '2',
                        taxPercent: 0,
                        taxType: 'fed'
                    }
                ]
            }
        };
        const outObject = taxes().country('can').quickBooksObject();
        regions.forEach(function(region) {
            expect(outObject[region]).to.deep.equal(object);
        });    
    });

    it('should return an object like this for AB, NT, NU, YT', function() {
        const regions = ['AB', 'NT', 'NU', 'YT'];
        const object = {
            provOnly: {
                taxCodeRef: "5",
                percentTotal: 5,
                taxes: [
                    {
                        taxRateRef: "10",
                        taxPercent: 5,
                        taxType: "fed"
                    }
                ]
            },
            fedOnly: {
                taxCodeRef: "5",
                percentTotal: 5, // GST only
                taxes: [
                    {
                        taxRateRef: "10",
                        taxPercent: 5,
                        taxType: "fed"
                    }
                ]
            },
            both: {
                taxCodeRef: "5",
                percentTotal: 5, // GST only
                taxes: [
                    {
                        taxRateRef: "10",
                        taxPercent: 5,
                        taxType: "fed"
                    }
                ]
            },
            none: {
                taxCodeRef: "2", // Tax-exempt
                percentTotal: 0,
                taxes: [
                    {
                        taxRateRef: "2",
                        taxPercent: 0,
                        taxType: "fed"
                    }
                ]
            }
        };
        const outObject = taxes().country('can').quickBooksObject();
        regions.forEach(function(region) {
            expect(outObject[region]).to.deep.equal(object);
        });
    });
});

describe('taxes.rate', function() {
    it('should return the correct tax rate', function() {
        const taxObject = taxes();
        expect(taxObject.country('can').region('on').rate()).to.be.equal(13);
        expect(taxObject.country('can').region('qc').rate()).to.be.equal(14.975);
    });
})
